describe('Model', () => {

  afterEach(() => {
    model.clearCircles();
  });

  it('addCircle should increment a number of circles', () => {
    model.addCircle('red');
    expect(model.getCircles().length).toBe(1)
  });

  it('addCircle should add circle with color specified', () => {
    var color = 'green';
    model.addCircle(color);
    expect(model.getCircles()).toContain({ id: 0, color: color });
  });

  describe('removeCircle', () => {
    beforeEach(() => {
      model.addCircle('green');
      model.addCircle('red');
      model.addCircle('blue');
    });

    it('removeCircle should decrement a number of circles', () => {
      model.removeCircle(1);
      expect(model.getCircles().length).toBe(2);
    });
  
    it('removeCircle should erase circle with specified id', () => {
      model.removeCircle(2);
      expect(model.getCircles()).not.toContain({ id: 2, color: 'blue' });
    });
  });

  describe('removeCirclesByColor', () => {
    beforeEach(() => {
      model.addCircle('green');
      model.addCircle('red');
      model.addCircle('red');
      model.addCircle('red');
      model.addCircle('blue');
    });

    it('should erase all red circles', () => {
      model.removeCirclesByColor('red');
      expect(model.getCircles()).not.toContain({ color: 'red' });
    });
  });
});