var controller = (function() {
  var addCircle = function(color) {
        var newCircle = model.addCircle(color);
        view.addCircle(newCircle, removeCircle);
      },

      clearCircles = function() {
        view.clearCircles();
      },

      removeCircle = function(event) {
        view.displayMessage('Circle removed');
        var removed = model.removeCircle(Number(event.target.id));
        view.removeCircle(removed.id);
      };

  return {
    'addCircle': addCircle,
    'clearCircles': clearCircles
  }
})();