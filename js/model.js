var model = (function() {
  var circles = [],
      id = 0;

      addCircle = function (color) {
        newCircle = {
          'id': id++,
          'color': color
        };

        circles.push(newCircle);

        return newCircle;
      },

      getCircles = function () {
        return circles;
      },

      removeCircle = function (id) {
        var removed = circles.find(circle => circle.id === id);
        circles = circles.filter(circle => circle.id !== id);

        return removed;
      },

      removeCirclesByColor = function(color) {
        circles = circles.filter(circle =>  circle.color !== color);
      },

      clearCircles = function () {
        circles = [];
        id = 0;
      }

  return {
    'addCircle': addCircle,
    'getCircles': getCircles,
    'removeCircle': removeCircle,
    'removeCirclesByColor': removeCirclesByColor,
    'clearCircles': clearCircles
  }
})();