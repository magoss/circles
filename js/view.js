var view = (function() {

  var addCircle = function(newCircle, clickCallback) {
        var circlesContainer = document.getElementById('circles'),
            newCircleElement = document.createElement('div'),
            inputColorValue = document.getElementById('circles-color-input').value || "red";

        newCircleElement.className = "circle " + inputColorValue;
        newCircleElement.addEventListener('click', clickCallback);
        newCircleElement.id = newCircle.id;
        circlesContainer.appendChild(newCircleElement);
      },

      clearCircles = function() {
        var circlesContainer = document.getElementById('circles').innerHTML = '';
      },

      removeCircle = function (id) {
        var circlesContainer = document.getElementById('circles'),
            circleElement = document.getElementById(id);
        circlesContainer.removeChild(circleElement);
      },

      displayMessage = function(message) {
        var messageContainer = document.getElementById('circles-message');
        messageContainer.innerText = message;
        setTimeout(function() {
          messageContainer.innerText = '';
        }, 2000);
      };

  return {
    'addCircle': addCircle,
    'clearCircles': clearCircles,
    'removeCircle': removeCircle,
    'displayMessage': displayMessage
  }
})();
